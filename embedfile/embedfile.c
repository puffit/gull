#include <stdlib.h>
#include <stdio.h>

static inline FILE *open_or_exit(const char *fname, const char *mode) {
	FILE *f = fopen(fname, mode);
	if (!f) {
		perror(fname);
		exit(EXIT_FAILURE);
	}
	return f;
}

int main(int argc, char **argv) {
	if (argc != 3) {
		fprintf(stderr, "USAGE: %s {outname} {infile}\n\n"
				"Creates {outname}.cc from the contents of {infile}\n",
				argv[0]);
		return EXIT_FAILURE;
	}

	const char *outname = argv[1];
	FILE *in = open_or_exit(argv[2], "r");

	char outpath[256];
	snprintf(outpath, sizeof(outpath), "%s.cc", outname);
	FILE *out = open_or_exit(outpath, "w");

	fprintf(out, "#include <string>\n"
			"std::string %s = {\n", outname);

	unsigned char buf[256];
	size_t nread = 0;
	do {
		nread = fread(buf, 1, sizeof(buf), in);
		for (size_t i = 0; i < nread; i++) {
			fprintf(out, "0x%02x,\n", buf[i]);
		}
	} while (nread > 0);
	fprintf(out, "};\n");

	fclose(in);
	fclose(out);

	return EXIT_SUCCESS;
}
