#include <cstdlib>
#include <memory>
#include <stdexcept>

#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include "make_program.hh"
#include "program.hh"
#include "shader.hh"
#include "vertexarray.hh"

constexpr int WIDTH = 800;
constexpr int HEIGHT = 600;

const std::vector<GLfloat> vertices = { 0.5f, 0.5f, 0.0f, 0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f, -0.5f, 0.5f, 0.0f };
const std::vector<GLuint> indices = { 0, 1, 3, 1, 2, 3 };

extern std::string basicFs;
extern std::string basicVs;

#include <iostream>

int main() {
	glfwInit();
	std::atexit(glfwTerminate);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	std::shared_ptr<GLFWwindow> window(
			glfwCreateWindow(WIDTH, HEIGHT, "gull", nullptr, nullptr),
			glfwDestroyWindow);
	if (!window) {
		throw std::runtime_error("Failed to initialise window");
	}

	glfwMakeContextCurrent(window.get());
	if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
		throw std::runtime_error("Failed to initialise context");
	}

	glfwSwapInterval(1);
	glViewport(0, 0, WIDTH, HEIGHT);
	glClearColor(1.f, 0.f, 1.f, 1.f);

	auto program = make_program(basicVs, basicFs);
	program->Bind();
	VertexArray triangle;
	triangle.LoadFloats(0, 3, vertices);
	triangle.LoadIndices(indices);

	double lastFrameTime = glfwGetTime();
	while (!glfwWindowShouldClose(window.get())) {
		double thisFrameTime = glfwGetTime();
		double deltaTime = thisFrameTime - lastFrameTime;
		lastFrameTime = thisFrameTime;

		glfwPollEvents();

		glClear(GL_COLOR_BUFFER_BIT);
		triangle.Bind();
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window.get());
	}

	return 0;
}
