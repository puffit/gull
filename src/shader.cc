#include "shader.hh"

#include <stdexcept>
#include <string>

#include <glad/glad.h>

Shader::Shader(GLenum type, const std::string &source) {
	this->id = glCreateShader(type);
	if (this->id == 0) {
		throw std::runtime_error("Failed to create shader");
	}

	const char *c_str = source.c_str();
	glShaderSource(this->id, 1, &c_str, nullptr);
	glCompileShader(this->id);
	int status;
	glGetShaderiv(this->id, GL_COMPILE_STATUS, &status);
	if (!status) {
		char error_log[512];
		glGetShaderInfoLog(this->id, 512, nullptr, error_log);
		throw std::runtime_error(
				"Failed to compile shader:\n" + std::string(error_log));
	}
}

Shader::~Shader() {
	glDeleteShader(this->id);
}
