#pragma once

#include <memory>
#include <string>

#include "program.hh"

std::shared_ptr<Program> make_program(const std::string &vertexSource, const std::string &fragmentSource);
