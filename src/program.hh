#pragma once

#include <vector>

#include <glad/glad.h>

#include "shader.hh"

class Program {
public:
	Program(const std::vector<Shader> &shaders);
	~Program();

	inline void Bind() const {
		glUseProgram(this->id);
	}

	inline GLuint GetId() const {
		return this->id;
	}

private:
	GLuint id;
};
