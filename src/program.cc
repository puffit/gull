#include "program.hh"

#include <stdexcept>
#include <vector>

#include <glad/glad.h>

#include "shader.hh"

Program::Program(const std::vector<Shader> &shaders) {
	this->id = glCreateProgram();
	if (this->id == 0) {
		throw std::runtime_error("Failed to create program");
	}

	for (auto &shader : shaders) {
		glAttachShader(this->id, shader.GetId());
	}

	glLinkProgram(this->id);
	int success;
	glGetProgramiv(this->id, GL_LINK_STATUS, &success);
	if (!success) {
		char error_log[512];
		glGetProgramInfoLog(this->id, 512, nullptr, error_log);
		throw std::runtime_error(
				"Failed to link program:\n" + std::string(error_log));
	}
}

Program::~Program() {
	glDeleteProgram(this->id);
	this->id;
}
