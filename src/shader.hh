#pragma once

#include <string>

#include <glad/glad.h>

class Shader {
public:
	Shader(GLenum type, const std::string &source);
	~Shader();

	inline GLuint GetId() const {
		return this->id;
	}
private:
	GLuint id;
};
