#pragma once

#include <vector>

#include <glad/glad.h>

class VertexArray {
public:
	VertexArray();
	~VertexArray();

	void LoadIndices(const std::vector<GLuint> &indices);
	void LoadFloats(int index, int size, const std::vector<GLfloat> &floats);

	inline void Bind() const {
		glBindVertexArray(this->id);
	}

	inline GLuint GetId() const {
		return this->id;
	}

	inline const std::vector<GLuint> &GetBuffers() const {
		return this->buffers;
	}

private:
	GLuint createBuffer();

	GLuint id;
	std::vector<GLuint> buffers;
};
