#include "vertexarray.hh"

#include <vector>

#include <glad/glad.h>

VertexArray::VertexArray() {
	glCreateVertexArrays(1, &(this->id));
}

VertexArray::~VertexArray() {
	glDeleteVertexArrays(1, &(this->id));
	glDeleteBuffers(this->buffers.size(), this->buffers.data());
}

GLuint VertexArray::createBuffer() {
	GLuint vbo;
	glCreateBuffers(1, &vbo);
	this->buffers.push_back(vbo);
	return vbo;
}

void VertexArray::LoadIndices(const std::vector<GLuint> &indices) {
	GLuint vbo = this->createBuffer();
	glNamedBufferData(vbo, indices.size() * sizeof(GLuint), indices.data(),
			GL_STATIC_DRAW);
	glVertexArrayElementBuffer(this->id, vbo);
}

void VertexArray::LoadFloats(int index, int size,
		const std::vector<GLfloat> &floats) {
	GLuint vbo = this->createBuffer();
	glNamedBufferData(vbo, floats.size() * sizeof(GLuint), floats.data(),
			GL_STATIC_DRAW);
	glEnableVertexArrayAttrib(this->id, index);
	glVertexArrayAttribFormat(this->id, index, size, GL_FLOAT, false, 0);
	glVertexArrayVertexBuffer(this->id, index, vbo, 0, size * sizeof(GLfloat));
	glVertexArrayAttribBinding(this->id, index, index);
}
