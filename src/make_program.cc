#include "make_program.hh"

#include <memory>
#include <string>
#include <vector>

#include <glad/glad.h>

#include "program.hh"
#include "shader.hh"

std::shared_ptr<Program> make_program(const std::string &vertexSource,
		const std::string &fragmentSource) {
	Shader vertexShader(GL_VERTEX_SHADER, vertexSource);
	Shader fragmentShader(GL_FRAGMENT_SHADER, fragmentSource);
	return std::make_shared<Program>(
			std::vector { vertexShader, fragmentShader });
}
